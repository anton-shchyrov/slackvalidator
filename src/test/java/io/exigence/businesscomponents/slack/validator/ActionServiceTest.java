package io.exigence.businesscomponents.slack.validator;

import io.exigence.businesscomponents.slack.validator.slack.response.CreateResponse;
import io.exigence.businesscomponents.slack.validator.slack.response.SlackError;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActionServiceTest {
    private static final String TOKEN = "xoxp-3358328880704-3327923872758-3359618028640-fa33cc943898a758ae58842977d3779b";

    @Test
    public void run() throws IOException, InterruptedException {
        ActionService service = new ActionService(TOKEN);
        service.run(false);
    }

    @Test
    public void createChannelFailed() throws IOException, InterruptedException {
        ActionService service = new ActionService("TOKEN");
        try {
            service.createChannel("test-channel", true);
            Assert.fail("Unexpected success");
        } catch (SlackError e) {
            Assert.assertEquals("Unexpected error", "invalid_auth", e.getMessage());
        }
        service = new ActionService(TOKEN);
        try {
            service.createChannel("InvalidChannel", true);
            Assert.fail("Unexpected success");
        } catch (SlackError e) {
            Assert.assertEquals("Unexpected error", "invalid_name_specials", e.getMessage());
        }
    }

    @Test
    public void sendMessageFailed() throws IOException, InterruptedException {
        ActionService service = new ActionService(TOKEN);
        try {
            service.sendMessage("invalid-channel", "Test Message");
            Assert.fail("Unexpected success");
        } catch (SlackError e) {
            Assert.assertEquals("Unexpected error", "channel_not_found", e.getMessage());
        }
    }

    @Test
    public void archiveChannelFailed() throws IOException, InterruptedException {
        ActionService service = new ActionService(TOKEN);
        try {
            service.archiveChannel("invalid-channel");
            Assert.fail("Unexpected success");
        } catch (SlackError e) {
            Assert.assertEquals("Unexpected error", "invalid_arguments", e.getMessage());
        }
    }
}