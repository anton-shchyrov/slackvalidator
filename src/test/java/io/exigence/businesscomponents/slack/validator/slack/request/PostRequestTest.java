package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Assert;
import org.junit.Test;

public class PostRequestTest extends AbstractRequestTest {
    private final String CHANNEL_NAME = "test-channel";

    @Test
    public void buildRequest() throws JsonProcessingException {
        final String message = "Test Message";

        PostRequest request = createTestObject();
        request.setText(message);
        JsonNode bodyNode = assertBody(request);
        Assert.assertEquals(2, bodyNode.size());
        assetJsonNode(bodyNode, "channel", CHANNEL_NAME);
        assetJsonNode(bodyNode, "text", message);
    }

    @Override
    protected PostRequest createTestObject() {
        return new PostRequest(TOKEN, CHANNEL_NAME);
    }

    @Override
    protected String methodName() {
        return "chat.postMessage";
    }
}