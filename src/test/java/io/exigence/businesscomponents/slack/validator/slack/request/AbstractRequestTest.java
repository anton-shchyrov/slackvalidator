package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractRequestTest {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    protected static final String TOKEN = "Token";

    private void assertHeader(String name, Map<String, List<String>> headers, String value) {
        String prefix = "Header " + name + " ";
        List<String> values = headers.get(name);
        Assert.assertNotNull(prefix + "not found", values);
        Assert.assertEquals(prefix + "more than one", 1, values.size());
        Assert.assertEquals(prefix + "contains unexpected value", value, values.get(0));
    }

    protected abstract AbstractRequest createTestObject();
    protected abstract String methodName();

    protected void assertRequestHeader(HttpRequest request) {
        Assert.assertEquals("Unexpected HTTP method", "POST", request.method());

        Map<String, List<String>> headers = request.headers().map();
        assertHeader("Content-Type", headers, "application/json; charset=UTF-8");
        assertHeader("Authorization", headers, "Bearer " + TOKEN);

        try {
            Assert.assertEquals("Unexpected URI", request.uri(), new URI("https://slack.com/api/" + methodName()));
        } catch (URISyntaxException e) {
            Assert.fail(e.getMessage());
        }

        Optional<HttpRequest.BodyPublisher> optionalBodyPublisher = request.bodyPublisher();
        Assert.assertTrue("Body is empty", optionalBodyPublisher.isPresent());
        Assert.assertTrue(
            "Unexpected length of body",
            optionalBodyPublisher.get().contentLength() > 0
        );
    }

    protected JsonNode assertBody(AbstractRequest request) throws JsonProcessingException {
        String bodyStr = request.buildBody();
        Assert.assertNotEquals("Body is empty", "", bodyStr);
        return MAPPER.readTree(bodyStr);
    }

    protected void assetJsonNode(JsonNode node, String name, String expected) {
        Assert.assertTrue(node.has(name));
        Assert.assertEquals(expected, node.get(name).asText());
    }

    @Test
    public void createRequest() throws JsonProcessingException {
        AbstractRequest request = createTestObject();
        HttpRequest httpRequest = request.createRequest();
        assertRequestHeader(httpRequest);
    }
}