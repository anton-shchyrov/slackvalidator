package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Assert;
import org.junit.Test;

public class CreateRequestTest extends AbstractRequestTest {
    private final String CHANNEL_NAME = "test-channel";

    @Test
    public void buildRequest() throws JsonProcessingException {
        CreateRequest request = createTestObject();
        JsonNode bodyNode = assertBody(request);
        Assert.assertEquals(1, bodyNode.size());
        assetJsonNode(bodyNode, "name", CHANNEL_NAME);
        //
        request.setPrivate(true);
        bodyNode = assertBody(request);
        Assert.assertEquals(2, bodyNode.size());
        assetJsonNode(bodyNode, "name", CHANNEL_NAME);
        Assert.assertTrue(bodyNode.has("is_private"));
        Assert.assertTrue(bodyNode.get("is_private").asBoolean());
    }

    @Override
    protected CreateRequest createTestObject() {
        return new CreateRequest(TOKEN, CHANNEL_NAME);
    }

    @Override
    protected String methodName() {
        return "conversations.create";
    }
}