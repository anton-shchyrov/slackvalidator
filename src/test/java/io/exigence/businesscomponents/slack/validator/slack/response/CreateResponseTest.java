package io.exigence.businesscomponents.slack.validator.slack.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

public class CreateResponseTest {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Test
    public void testOk() throws JsonProcessingException {
        String jsonStr = "{\"ok\": true, \"channel\": {\"id\": \"channel-id\", \"name\": \"channel-name\", \"other\": \"value\"}}";
        CreateResponse response = MAPPER.readValue(jsonStr, CreateResponse.class);
        Assert.assertEquals("channel-id", response.getId());
        Assert.assertEquals("channel-name", response.getName());
    }

    @Test
    public void testFailed() throws JsonProcessingException {
        final String error = "Test Error";
        String jsonStr = String.format("{\"ok\": false, \"error\": \"%s\"}", error);
        try {
            MAPPER.readValue(jsonStr, CreateResponse.class);
            Assert.fail("Expected error");
        } catch (SlackError e) {
            Assert.assertEquals(error, e.getMessage());
        }
    }
}