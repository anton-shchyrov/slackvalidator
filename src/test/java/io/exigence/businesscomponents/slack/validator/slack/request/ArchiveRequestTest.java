package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Assert;
import org.junit.Test;

public class ArchiveRequestTest extends AbstractRequestTest {
    private final String CHANNEL_NAME = "test-channel";

    @Test
    public void buildRequest() throws JsonProcessingException {
        ArchiveRequest request = createTestObject();
        JsonNode bodyNode = assertBody(request);
        Assert.assertEquals(1, bodyNode.size());
        assetJsonNode(bodyNode, "channel", CHANNEL_NAME);
    }

    @Override
    protected ArchiveRequest createTestObject() {
        return new ArchiveRequest(TOKEN, CHANNEL_NAME);
    }

    @Override
    protected String methodName() {
        return "conversations.archive";
    }
}