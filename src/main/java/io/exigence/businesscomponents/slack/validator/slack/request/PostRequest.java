package io.exigence.businesscomponents.slack.validator.slack.request;

public class PostRequest extends ChannelRequest {
    private String text;

    public PostRequest(String token, String channel) {
        super(token, channel);
    }

    @Override
    protected String getMethodName() {
        return "chat.postMessage";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
