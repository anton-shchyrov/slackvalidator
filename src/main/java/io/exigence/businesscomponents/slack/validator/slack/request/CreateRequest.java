package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateRequest extends AbstractRequest {
    private final String name;
    @JsonProperty(value = "is_private", defaultValue = "false")
    private boolean isPrivate;

    public CreateRequest(String token, String name) {
        super(token);
        this.name = name;
    }

    @Override
    protected String getMethodName() {
        return "conversations.create";
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
