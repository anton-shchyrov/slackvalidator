package io.exigence.businesscomponents.slack.validator.slack.response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = EmptyResponseDeserializer.class)
public class EmptyResponse {
}

class EmptyResponseDeserializer extends SlackDeserializer<EmptyResponse> {
    @Override
    protected EmptyResponse deserializeBody(JsonNode node) {
        return new EmptyResponse();
    }
}
