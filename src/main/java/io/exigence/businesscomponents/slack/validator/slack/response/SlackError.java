package io.exigence.businesscomponents.slack.validator.slack.response;

public class SlackError extends Error {
    public SlackError(String message) {
        super(message);
    }
}
