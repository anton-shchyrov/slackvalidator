package io.exigence.businesscomponents.slack.validator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.exigence.businesscomponents.slack.validator.slack.request.ArchiveRequest;
import io.exigence.businesscomponents.slack.validator.slack.request.CreateRequest;
import io.exigence.businesscomponents.slack.validator.slack.request.PostRequest;
import io.exigence.businesscomponents.slack.validator.slack.response.CreateResponse;
import io.exigence.businesscomponents.slack.validator.slack.response.EmptyResponse;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActionService {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final HttpClient client = HttpClient.newHttpClient();;
    private final String token;

    public ActionService(String token) {
        this.token = token;
    }

    private <T> T call(HttpRequest request, Class<T> clazz) throws IOException, InterruptedException {
        int counter = 2;
        while (true) {
            try {
                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                String responseBody = response.body();
                return MAPPER.readValue(responseBody, clazz);
            } catch (Throwable t) {
                counter--;
                if (counter == 0)
                    throw t;
            }
        }
    }

    public CreateResponse createChannel(String name, boolean isPrivate) throws IOException, InterruptedException {
        CreateRequest createRequest = new CreateRequest(token, name);
        createRequest.setPrivate(isPrivate);
        return call(createRequest.createRequest(), CreateResponse.class);
    }

    public void sendMessage(String channel, String message) throws IOException, InterruptedException {
        PostRequest postRequest = new PostRequest(token, channel);
        postRequest.setText(message);
        call(postRequest.createRequest(), EmptyResponse.class);
    }

    public void archiveChannel(String channel) throws IOException, InterruptedException {
        ArchiveRequest archiveRequest = new ArchiveRequest(token, channel);
        call(archiveRequest.createRequest(), EmptyResponse.class);
    }

    public void run(boolean isPrivate) throws IOException, InterruptedException {
        String channelName = "test-channel-" +
            (isPrivate ? "p-" : "") +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss"));
        CreateResponse createResponse = createChannel(channelName, isPrivate);
        System.out.printf("Channel %s (%s) was created%n", createResponse.getName(), createResponse.getId());

        sendMessage(createResponse.getId(), "Text message");
        System.out.println("Message sent");

        archiveChannel(createResponse.getId());
        System.out.println("Channel archived");
    }
}
