package io.exigence.businesscomponents.slack.validator.slack.request;

public class ArchiveRequest extends ChannelRequest {
    public ArchiveRequest(String token, String channel) {
        super(token, channel);
    }

    @Override
    protected String getMethodName() {
        return "conversations.archive";
    }
}
