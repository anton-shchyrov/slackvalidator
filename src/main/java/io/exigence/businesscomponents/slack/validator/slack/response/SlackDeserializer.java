package io.exigence.businesscomponents.slack.validator.slack.response;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public abstract class SlackDeserializer<T> extends StdDeserializer<T> {
    protected SlackDeserializer() {
        this(null);
    }

    protected SlackDeserializer(Class<?> vc) {
        super(vc);
    }

    protected String decodeErrorMessage(String error) {
        return error;
    }

    protected abstract T deserializeBody(JsonNode node);

    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        boolean isOk = node.get("ok").asBoolean();
        if (!isOk) {
            String error = node.get("error").asText();
            throw new SlackError(decodeErrorMessage(error));
        }
        return deserializeBody(node);
    }
}
