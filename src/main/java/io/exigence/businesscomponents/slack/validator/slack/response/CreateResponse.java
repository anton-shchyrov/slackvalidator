package io.exigence.businesscomponents.slack.validator.slack.response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = CreateResponseDeserializer.class)
public class CreateResponse {
    private final String id;
    private final String name;

    public CreateResponse(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

class CreateResponseDeserializer extends SlackDeserializer<CreateResponse> {
    @Override
    protected CreateResponse deserializeBody(JsonNode node) {
        JsonNode channelInfo = node.get("channel");
        String id = channelInfo.get("id").asText();
        String name = channelInfo.get("name").asText();
        return new CreateResponse(id, name);
    }
}