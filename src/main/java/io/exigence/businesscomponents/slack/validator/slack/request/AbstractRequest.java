package io.exigence.businesscomponents.slack.validator.slack.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;

public abstract class AbstractRequest {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    protected static final String API_URL = "https://slack.com/api/";
    @JsonIgnore
    private final String token;
    @JsonIgnore
    private final URI uri;

    static {
        MAPPER.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        MAPPER.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        MAPPER.setDefaultPropertyInclusion(JsonInclude.Include.NON_DEFAULT);
    }

    public AbstractRequest(String token) {
        this.token = token;
        try {
            uri = new URI(API_URL + getMethodName());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract String getMethodName();

    protected String buildBody() throws JsonProcessingException {
        return MAPPER.writeValueAsString(this);
    }

    public HttpRequest createRequest() throws JsonProcessingException {
        String request = buildBody();
        return HttpRequest.newBuilder(uri)
            .header("Content-type", "application/json; charset=UTF-8")
            .header("Authorization", "Bearer " + token)
            .POST(HttpRequest.BodyPublishers.ofString(request))
            .build();
    }
}
