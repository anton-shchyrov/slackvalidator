package io.exigence.businesscomponents.slack.validator.slack.request;

public abstract class ChannelRequest extends AbstractRequest {
    private final String channel;

    public ChannelRequest(String token, String channel) {
        super(token);
        this.channel = channel;
    }
}
